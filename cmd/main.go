package main

import (
	git "git-sudo/pkg"
	"os"
	"strings"
)

import (
	"fmt"
)

func parseCommand() []string {
	args := os.Args
	command := args[5:]
	return command
}

func main() {
	command := parseCommand()
	config := git.Configuration
	localSettings := git.LocalConfig
	globalSettings := git.GlobalConfig

	oldLocalUser := localSettings.UserName()
	oldLocalEmail := localSettings.Email()
	oldGlobalUser := globalSettings.UserName()
	oldGlobalEmail := globalSettings.Email()

	localSettings.SetUserName(config.User)
	localSettings.SetEmail(config.Email)


	fmt.Printf("executing: 'git %v'\n", command)

	_, err := git.Execute(command...)
	localSettings.SetUserName(oldLocalUser)
	localSettings.SetEmail(oldLocalEmail)
	globalSettings.SetUserName(oldGlobalUser)
	globalSettings.SetEmail(oldGlobalEmail)

	if err != nil {
		fmt.Printf("'git %v' failed", strings.Join(command, " "))
		os.Exit(1)
	}

}

/*
	git-sudo -u test.com -e test@email.com add . test
*/
