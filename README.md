# Git Sudo

A sudo-clone but for Git. All changed names, emails are applied locally 
and reverted at the end of execution. 

## Logging

Use the `LOG_LEVEL` environment parameter to adjust logging verbosity

Supported log levels are:

    	OFF (default)
    	WARN
    	DEBUG
    	TRACE


## Example Usage

    git-sudo -u "Mr. Rogers" -e mrrogers@pbs.org commit -m "A commit msg"
    # subsequent commands run as the previous user

Returns

    commit 1874f7e0544969966da6e038b15d6b743ea37f5f (HEAD -> master)
    Author: Mr. Rogers <mrrogers@pbs.org>
    Date:   Thu Feb 13 13:33:45 2020 -0500
    
        A commit msg
    

