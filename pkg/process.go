package pkg

import (
	"log"
	"os/exec"
	"strings"
)

func Execute(args ...string) (string, error) {
	out, err := exec.Command("git", args...).Output()
	Logger.Trace("executing 'git %v'", strings.Join(args, " "))
	return string(out), err
}

func ExecuteF(args ...string) string {
	if out, err := Execute(args...); err != nil {
		log.Fatalf("run into errors running git '%v', see %v\n", strings.Join(args, " "), err)
		return ""
	} else {
		return out
	}
}


func trimmedExecuteF(args ...string) string {
	out := ExecuteF(args...)
	return strings.TrimSpace(out)
}
