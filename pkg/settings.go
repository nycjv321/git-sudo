package pkg

import (
	"flag"
	"os"
)

type ApplicationConfiguration struct {
	User  string
	Email string
}

var Configuration ApplicationConfiguration

func validateFlags(user string, email string) {
	if len(user) == 0 {
		if _, err := os.Stderr.WriteString("please provide a user (-u) to sudo as\n"); err != nil {
			panic(err)
		} else {
			os.Exit(0)
		}
	}

	if len(email) == 0 {
		if _, err := os.Stderr.WriteString("please provide an email (-e) to sudo as\n"); err != nil {
			panic(err)
		} else {
			os.Exit(0)
		}
	}
}

func init() {
	var user = flag.String("u", "", "The -u (user) option causes git-sudo to run the git operation to execute as the provided user")
	var email = flag.String("e", "", "The -e (email) option causes git-sudo to run the git operation to execute as the provided user")
	flag.Parse()
	validateFlags(*user, *email)

	Configuration = ApplicationConfiguration{*user, *email}
}
