package pkg

import (
	"log"
	"os"
	"strings"
)

type LogLevel int

var Logger LogLevel

const (
	OFF LogLevel = 1 + iota
	WARN
	DEBUG
	TRACE
)

func readLogLevel() LogLevel {
	logLevel := os.Getenv("LOG_LEVEL")
	if len(logLevel) == 0 {
		return OFF
	} else {
		return parseLogLevel(logLevel)
	}
}

func init() {
	Logger = readLogLevel()
}

func (l LogLevel) Debug(msg string, args ...interface{}) {
	if l >= DEBUG {
		if msg[len(msg)-1] != '\n' {
			log.Printf(msg+"\n", args)
		} else {
			log.Printf(msg, args)
		}
	}
}

func (l LogLevel) Trace(msg string, args ...interface{}) {
	if l == TRACE {
		if msg[len(msg)-1] != '\n' {
			log.Printf(msg+"\n", args)
		} else {
			log.Printf(msg, args)
		}
	}
}

func parseLogLevel(level string) LogLevel {
	switch strings.TrimSpace(strings.ToUpper(level)) {
	case "WARN":
		return WARN
	case "DEBUG":
		return DEBUG
	case "TRACE":
		return TRACE
	default:
		return OFF
	}
}
