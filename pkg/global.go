package pkg

type GlobalSettings struct{}

var GlobalConfig = GlobalSettings{}

func (GlobalSettings) UserName() string {
	userName := trimmedExecuteF("config", "--global", "user.name")
	Logger.Debug("global name (config user.name) is set to %v", userName)
	return userName
}

func (GlobalSettings) SetUserName(userName string) string {
	Logger.Debug("setting global name (config user.name) to %v", userName)
	return trimmedExecuteF("config", "--global", "user.name", userName)
}

func (GlobalSettings) SetEmail(email string) string {
	Logger.Debug("setting global name (config user.email) to %v", email)
	return trimmedExecuteF("config", "--global", "user.email", email)
}

func (GlobalSettings) Email() string {
	email := trimmedExecuteF("config", "--global", "user.email")
	Logger.Debug("global name (config user.email) is set to %v", email)

	return email
}
