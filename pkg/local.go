package pkg

type LocalSettings struct{}

var LocalConfig = LocalSettings{}

func (LocalSettings) UserName() string {
	userName := trimmedExecuteF("config", "user.name")
	if len(userName) == 0 {
		Logger.Debug("local name (config user.name) is not set")
	} else {
		Logger.Debug("local name (config user.name) is set to %v\n", userName)
	}
	return userName
}

func (LocalSettings) SetUserName(userName string) string {
	Logger.Debug("Setting username (config user.name) to %v\n", userName)
	return trimmedExecuteF("config", "user.name", userName)
}

func (LocalSettings) SetEmail(userName string) string {
	Logger.Debug("Setting email (config user.email) to %v\n", userName)
	return trimmedExecuteF("config", "user.email", userName)
}

func (LocalSettings) Email() string {
	email := trimmedExecuteF("config", "user.email")
	if len(email) == 0 {
		Logger.Debug("local email (config user.email) is not set")
	} else {
		Logger.Debug("local email (config user.email) is set to %v\n", email)
	}
	return email
}
